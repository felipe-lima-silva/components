# Introdução
Nesta seção mostraremos como instalar os componentes padrões que utilizamos nos clusters de Kubernetes. Lembre-se, alguns são opcionais, e outros podem não ser necessários. 

# Conectando a um cluster EKS
aws eks --region <regiao> update-kubeconfig --name <nome_do_cluster>

# Helm
### Instalação do Helm v3
```sh
wget https://get.helm.sh/helm-v3.2.4-linux-amd64.tar.gz

tar -zxvf helm-v3.2.4-linux-amd64.tar.gz

sudo cp linux-amd64/helm /usr/bin/

sudo chmod +x /usr/bin/helm

helm version
```


### Comandos Básicos do Helm
```sh
helm create api-demo

helm repo add <endereco_do_repositorio>

helm repo update

helm search repo prometheus

helm install <nome_da_aplicacao> /api-demo/

helm upgrade api-demo /api-demo --set version=v2

helm rollback <nome_do_chart> <numero_da_revisao>

helm status

helm delete <nome_da_release>

helm list

helm history <nome_do_chart>
```
***

# Metrics server
O componente de servidor de métricas do K8S é essencial para o funcionamento de outros componentes como HPA e Kubernetes Dashboard.
A instalação do metrics server é bem simples.
1. Acesse o Manager Server que tem acesso ao Cluster EKS.
1. Dê os seguinte comandos:
```sh
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.7/components.yaml

kubectl get deployment metrics-server -n kube-system
```
+ Saída esperada do último comando:
```sh
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
metrics-server   1/1     1            1           6m
``` 

# Cluster Auto Scaler

- Coloque as seguintes tags no Auto Scaling Group dos Workers do EKS:

**chave**                                  **valor**
k8s.io/cluster-autoscaler/<nome-do-cluster>  owned
k8s.io/cluster-autoscaler/enabled            TRUE

- Acesse o manager server que tem acesso ao Cluster EKS.
- Crie um arquivo chamado cluster-autoscaler-policy.json
- Copie o seguinte conteúdo
```sh
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ec2:DescribeLaunchTemplateVersions"
            ],
            "Resource": "*",
            "Effect": "Allow"
        }
    ]
}
```
- Salve o arquivo.
- Dê o seguinte comando para criar uma IAM Policy.  Substitua <valor>,(incluindo <>), pelo nome da sua Policy, ou deixe o mesmo nome que está, retirando apenas os <>.
```sh
aws iam create-policy \
    --policy-name <AmazonEKSClusterAutoscalerPolicy> \
    --policy-document file://cluster-autoscaler-policy.json
```
- Entre na conta onde foi subido o cluster do EKS. Será necessário criar uma role.
- Clique em Create Role. Em **Select type of trusted entity**, escolha **Web identity**.
- Em **Choose a web identity provider**, coloque **Identity provider** com a URL do cluster EKS, e coloque **Audience** com sts.amazonaws.com.
- Avançe, e na parte de permissões da role escolha a Policy que foi criada anteriormente.
- Avançe, pule a parte de tags. Coloque o nome da sua role, como AmazonEKSClusterAutoscalerRole. Clique no botão de criação de Role.
- Na console, abra a Role que acabou de ser criada.
- Na parte de **Trust relationships**, escolha **Edit trust relationship**.
- Ache uma linha similar a:

```sh
"oidc.eks.us-west-2.amazonaws.com/id/EXAMPLED539D4633E53DE1B716D3041E:aud": "sts.amazonaws.com"
```

- Substitua a linha pelo conteúdo abaixo, substituindo os elemntos em <> pela região e ID do OIDC (id/<>).

```sh
"oidc.eks.<region-code>.amazonaws.com/id/<EXAMPLED539D4633E53DE1B716D3041E>:sub": "system:serviceaccount:kube-system:cluster-autoscaler"
```

- Clique para atualizar a Trust policy.
- Anote o ARN da Role criada.
- Acesse o manager server que tem acesso ao Cluster EKS.
- Dê o seguinte comando:
```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml
```
- Coloque a Annotation na service Account do cluster-autoscaler o ARN da Role criada anteriormente através do seguinte comando:
```sh
kubectl annotate serviceaccount cluster-autoscaler \
  -n kube-system \
  eks.amazonaws.com/role-arn=<ARN-DA-ROLE>
```
- Dê o seguinte comando:
```sh
kubectl patch deployment cluster-autoscaler \
  -n kube-system \
  -p '{"spec":{"template":{"metadata":{"annotations":{"cluster-autoscaler.kubernetes.io/safe-to-evict": "false"}}}}}'
```
- Dê o seguinte comando para editar o Deployment:
```sh
kubectl -n kube-system edit deployment.apps/cluster-autoscaler
```
- Edite os comandos do container cluster-autoscaler, colocando o nome do seu cluster em <YOUR CLUSTER NAME>, e adicionando dois argumentos: --balance-similar-node-groups e --skip-nodes-with-system-pods=false. Resultado esperado da edição:
```sh
    spec:
      containers:
      - command:
        - ./cluster-autoscaler
        - --v=4
        - --stderrthreshold=info
        - --cloud-provider=aws
        - --skip-nodes-with-local-storage=false
        - --expander=least-waste
        - --node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/<YOUR CLUSTER NAME>
        - --balance-similar-node-groups
        - --skip-nodes-with-system-pods=false
```
- Abra o link de releases do Cluster Auto Scaler e ache a versão correspondente a do seu cluster [cluster autoscaler](https://github.com/kubernetes/autoscaler/releases/ "releases cluster autoscaler"). Se a versão do cluster for 1.18, ache a versão 1.18.x mais recente possível. Lembrando que a release está com o prefixo de "Cluster Auto Scaler".
- Dê o seguinte coando, substituindo apenas <1.18.n> pela versão escolhida do cluster auto scaler.
```sh
kubectl set image deployment cluster-autoscaler \
  -n kube-system \
  cluster-autoscaler=k8s.gcr.io/autoscaling/cluster-autoscaler:v<1.18.n>
```
- Para ver os logs, dê o seguinte comnado:
```sh
kubectl -n kube-system logs -f deployment.apps/cluster-autoscaler
```
# ALB Ingress Controller
Para utilizar o Load Balancer com o Kubernetes, é necessário essse componente. Se você precisa de um Load Balancer operando na na camada 7 (HTTP/HTTPS), criado e gerenciado totalmente pelo Kubernetes utilize esse componente. 
É necessário um IAM OIDC para utilizar este componente. Por default a stack do repositório eks já cria esse recurso.O ALB Ingress Controller cria as regras de redirecionamento nos próprios listeners.  Link para instalação: https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html.

1. Acesse o Manager Server do Cluster Kubernetes.
1. Dê o seguinte comando fazendo as substituições necessárias em <valor>, (incluindo <>), para que seja verificado se existe um IAM OIDC provider para o cluster EKS.

```
aws eks describe-cluster --name <nome_do_cluster> --query "cluster.identity.oidc.issuer" --output text

```

+ Saída esperada:

```sh
https://oidc.eks.us-west-2.amazonaws.com/id/EXAMPLED539D4633E53DE1B716D3041E
```

1. Liste os IAM OIDC providers dentro da sua conta. Substitua <valor>,(incluindo <>), pelos caracteres adquiridos na saída do comando anterior após id/.

```
aws iam list-open-id-connect-providers | grep <EXAMPLED539D4633E53DE1B716D3041E>
```

+ Saída esperada:

```sh
"Arn": "arn:aws:iam::111122223333:oidc-provider/oidc.eks.us-west-2.amazonaws.com/id/EXAMPLED539D4633E53DE1B716D3041E"
```

1. Dê o seguinte comando para baixar um arquivo JSON de uma IAM Policy para todas as regiões, menos as chinesas.
 
```sh
curl -o iam_policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.0/docs/install/iam_policy.json
```

1. Dê o seguinte comando para criar uma IAM Policy.  Substitua <valor>,(incluindo <>), pelo nome da sua Policy, ou deixe o mesmo nome que está, retirando apenas os <>.

```sh
aws iam create-policy \
    --policy-name <AWSLoadBalancerControllerIAMPolicy> \
    --policy-document file://iam_policy.json
```

1. Entre na conta onde foi subido o cluster do EKS. Será necessário criar uma role.
1. Clique em Create Role. Em **Select type of trusted entity**, escolha **Web identity**.
1. Em **Choose a web identity provider**, coloque **Identity provider** com a URL do cluster EKS, e coloque **Audience** com sts.amazonaws.com.
1. Avançe, e na parte de permissões da role escolha a Policy que foi criada anteriormente.
1. Avançe, pule a parte de tags. Coloque o nome da sua role, como AmazonEKSLoadBalancerControllerRole. Clique no botão de criação de Role.
1. Na console, abra a Role que acabou de ser criada.
1. Na parte de **Trust relationships**, escolha **Edit trust relationship**.
1. Ache uma linha similar a:

```sh
"oidc.eks.us-west-2.amazonaws.com/id/EXAMPLED539D4633E53DE1B716D3041E:aud": "sts.amazonaws.com"
```

1. Substitua a linha pelo conteúdo abaixo, substituindo os elemntos em <> pela região e ID do OIDC.

```sh
"oidc.eks.<region-code>.amazonaws.com/id/<EXAMPLED539D4633E53DE1B716D3041E>:sub": "system:serviceaccount:kube-system:aws-load-balancer-controller" 
```

1. Clique para atualizar a Trust policy.
1. Anote o ARN da Role criada.
1. Acesse novamente o manager server e salve o conteúdo abaixo em um arquivo chamado aws-load-balancer-controller-service-account.yaml.

```sh
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/name: aws-load-balancer-controller
  name: aws-load-balancer-controller
  namespace: kube-system
  annotations:
      eks.amazonaws.com/role-arn: arn:aws:iam::<AWS_ACCOUNT_ID>:role/AmazonEKSLoadBalancerControllerRole
```
+

1. Substitua no arquivo a parte de Account ID e se necessário a parte de nome da Role criada.
1. Dê o seguinte comando

```sh
kubectl apply -f aws-load-balancer-controller-service-account.yaml
```

1. Vamos instalar agora o AWS Load Balancer Controller. 
- - Primeiramente é necessário instalar o cert-manager. Nesse recursos existe uma variação dependendo da versão do K8S. 
+ Versão do Kubernetes: 1.16 +

  ```
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.2/cert-manager.yaml
  ```
+

 +  Versão do Kubernetes: 1.15

 ```
 kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.2/cert-manager-legacy.yaml
  ```
 +
- - Agora vamos instalar o controller. Baixe o arquivo YAMl através desse comando.

```
curl -o v2_1_0_full.yaml https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.0/docs/install/v2_1_0_full.yaml
```
1. Abra o arquivo v2_1_0_full.yaml, e remova a seguinte seção dele:
```
---
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/name: aws-load-balancer-controller
  name: aws-load-balancer-controller
  namespace: kube-system
```

1. Após remover esse conteúdo do arquivo, ache a linha com o parâmetro --cluster-name . Substitua o valor do parâmetro pelo nome do cluster.
1. Aplique o arquivo.

```
kubectl apply -f v2_1_0_full.yaml
```
- Fonte do conteúdo acima: https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html
- Coloque a seguinte tag nas subnets públicas do cluster, se for utilizar um ALB Externo/Público: chave - kubernetes.io/role/elb | valor - 1 .
- Coloque a seguinte tag nas subnets privadas do cluster, se for utilizar um ALB Interno/Privado:  chave - kubernetes.io/role/internal-elb | valor - 1 .
- Para verificar se o ALB Ingress Controller está funcionando, aplique o seguinte comando para subir uma aplicação do 2048 de exemplo:

```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.0/docs/examples/2048/2048_full.yaml
```
- Dê o seguinte comando para verificar se o ingress subiu:

```sh
kubectl get ingress/ingress-2048 -n game-2048
```
Saída esperada:

```sh
NAME           CLASS    HOSTS   ADDRESS                                                                   PORTS   AGE
ingress-2048   <none>   *       k8s-game2048-ingress2-xxxxxxxxxx-yyyyyyyyyy.us-west-2.elb.amazonaws.com   80      2m32s
```

- Acesse o endereço do Load Balancer criado na coluna ADDRESS.
- Para subir uma imagem personalizada, crie dois arquivos no Manager Server: svc.YAML e ingress.YAML .
- Copie o conteúdo dos arquivos svc.YAML e ingress.YAML desse repositório nos respectivos arquivos no manager.
- Os key/values nas labels devem ter a mesma chave e valor. Isso é essencial para a aplicação funcionar. No parta do service do K8S o intervalo deve ser de 30000 a 32767.
- Edite os arquivos, substituindo o valor de todos os campos conforme o necessário.
- Dê dois comandos de kubectl apply -f:

```sh
kubectl apply -f svc.YAML
kubectl apply -f ingress.YAML
```

# NGINX Ingress Controller 
Para utilizar o Network Load Balancer com o Kubernetes é necessário esse componente. O NGINX Ingress Controller cria as regras de redirecionamento da aplicação dentro do NGINX. 
Se precisar de um Load Balancer na camada 4 (TCP) gerenciado pelo Kubernetes, utilize esse componente.O NGINX ingress controler cria as regras de redirecionamento em um arquivo de configuração do próprio NGINX.
Existem duas opções:

- Sem TLS (Certificado Digital)

1. Acesse o Manager Server que tem acesso ao cluser EKS.
1. Dê o seguinte comando:

```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.43.0/deploy/static/provider/aws/deploy.yaml
```

1. Após criado o load balancer, pegue o seu endereço DNS e utilize o comando ping para descobrir o seu IP. Comando: ping <DNS-LB>.
1. Anote o IP.
1. Vá até o Route 53 do domínio e crie um registro do tipo CNAME com o nome do domínio escolhido que aponta para o IP do Load Balancer.
- Para subir uma imagem personalizada, crie dois arquivos no Manager Server: svc.YAML e ingress.YAML .
- Copie o conteúdo dos arquivos svc.YAML e ingress.YAML desse repositório nos respectivos arquivos no manager.
- Os key/values nas labels devem ter a mesma chave e valor. Isso é essencial para a aplicação funcionar. No parta do service do K8S o intervalo deve ser de 30000 a 32767.
- Edite os arquivos, substituindo o valor de todos os campos conforme o necessário.
- Dê dois comandos de kubectl apply -f:
```sh
kubectl apply -f svc.YAML
kubectl apply -f ingress.YAML
```

- Com TLS (Certificado Digital)

1. Acesse o Manager Server que tem acesso ao cluser EKS.
1. Dê o seguinte comando para baixar o arquivo YAML:

```sh
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.43.0/deploy/static/provider/aws/deploy-tls-termination.yaml
```

1. Verifique se existe um certificado do domínio que será utilizado dentro do serviço ACM, na mesma região do cluster.Se existir, copie o ARN, se não crie um certificado do ACM.
1. Edite o arquivo deploy-tls-termination.yaml, e procure dentro do arquivo pelo parâmetro proxy-real-ip-cidr . Coloque como valor o CIDR da VPC do Cluster EKS.
1. Continuando na edição do arquivo, procure por um ARN genérico do ACM (arn:aws:acm:us-west-2:XXXXXXXX:certificate/XXXXXX-XXXXXXX-XXXXXXX-XXXXXXXX), e substitua pelo ARN do certificado do ACM que será utilizado.
1. Salve o arquivo, e digite o seguinte comando:

```
kubectl apply -f deploy-tls-termination.yaml
```

1. Aguarde a criação do Load Balancer.
1. Após criado o load balancer, pegue o seu endereço DNS e utilize o comando ping para descobrir o seu IP. Comando: ping <DNS-LB>.
1. Anote o IP.
1. Vá até o Route 53 do domínio e crie um registro do tipo CNAME com o nome do domínio escolhido que aponta para o IP do Load Balancer ou coloque o IP do load balancer apontando para o domínio escolhido no arquivo hosts do Windows.

- Para subir uma imagem personalizada, crie dois arquivos no Manager Server: svc.YAML e ingress.YAML .
- Copie o conteúdo dos arquivos svc.YAML e ingress.YAML desse repositório nos respectivos arquivos no manager.
- Os key/values nas labels devem ter a mesma chave e valor. Isso é essencial para a aplicação funcionar. No parta do service do K8S o intervalo deve ser de 30000 a 32767.
- Edite os arquivos, substituindo o valor de todos os campos conforme o necessário.
- Dê dois comandos de kubectl apply -f:

```sh
kubectl apply -f svc.YAML
kubectl apply -f ingress.YAML